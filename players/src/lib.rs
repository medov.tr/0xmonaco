#![no_std]
use gstd::{exec, msg, prelude::*, ActorId};
use monaco_io::*;

// TODO: ПРОВЕРКА БАЛАНСА!!!!!
fn get_position(cars: Vec<Car>, owner_car: &Car) -> usize{
    let owner_location = owner_car.position;
    let mut all_location = Vec::new();
    for car in cars.iter(){
        all_location.push(car.position);
    }
    all_location.sort();
    all_location.iter().position(|&x| x == owner_location).unwrap()
}

#[gstd::async_main]
async fn main() {

    let race_id = msg::source();
    let message: YourTurn = msg::load().expect("Unable to decode struct`YourTurn`");

    let my_player = message.index_owner;
    let message_copy = message.clone();
    let my_car = message_copy.cars.get(my_player as usize).unwrap();

    let position = get_position(message.cars.clone(), my_car);
    if position == 0 {

        let reply:RaceEvent = msg::send_for_reply_as(
            race_id,
            RaceAction::BuyAcceleration { speed: 3 },
            0,
        )
        .expect("Error in sending a message `GameAction::BuyAcceleration`")
        .await
        .expect("Unable to decode `RaceEvent");
    }
    else {

        let reply_1:RaceEvent = msg::send_for_reply_as(
            race_id,
            RaceAction::BuyBomb { num_bomb: 1 },
            0,
        )
        .expect("Error in sending a message `GameAction::BuyBomb`")
        .await
        .expect("Unable to decode `RaceEvent");

        let reply_2:RaceEvent = msg::send_for_reply_as(
            race_id,
            RaceAction::BuyAcceleration { speed: 3 },
            0,
        )
        .expect("Error in sending a message `GameAction::BuyAcceleration`")
        .await
        .expect("Unable to decode `RaceEvent");
    }

    msg::reply("", 0).expect("Error in sending a reply to race contract");

}

#[no_mangle]
unsafe extern "C" fn init() {
    //   MONOPOLY = msg::load::<ActorId>().expect("Unable to decode ActorId");
}









