#![no_std]

use codec::{Decode, Encode};
use gstd::{prelude::*, ActorId};
use scale_info::TypeInfo;

#[derive(Encode, Decode, TypeInfo, Clone)]
pub struct YourTurn {
    pub cars: Vec<Car>,
    pub index_owner: u16,
}

#[derive(Encode,Decode, Debug, Default, Clone, PartialEq, TypeInfo)]
pub struct Car{
    pub number: u128,
    pub balance: u128,
    pub speed: u128,
    pub position: u128,
}

#[derive(Debug, Default, Encode, Decode, TypeInfo, Clone, PartialEq, Eq,)]
pub enum State {
    #[default] WAITING,
    PLAY,
    FINISHED,
}

#[derive(Debug, Encode, Decode, TypeInfo)]
pub enum Action {
    ACCELERATE,
    BOMB,
}

#[derive(Debug, Default, Encode, Decode, TypeInfo)]
pub struct InitRace {
    pub admin: ActorId,
}

#[derive(Debug, Encode, Decode, TypeInfo)]
pub enum RaceAction {
    Register {player: ActorId},
    ReserveGas,
    BuyAcceleration{speed: u128},
    BuyBomb{num_bomb: u128},
    Play,
}


#[derive(Debug, Encode, Decode, TypeInfo)]
pub enum RaceEvent {
    RaceRegistered{
        race_id: ActorId,
        player: ActorId,

    },
    NextRoundFromReservation,
    GasReserved,

    Step{
        players_queue: Vec<ActorId>,
        cars_queue: Vec<Car>,
        current_player: ActorId,
    },
    Accelerated{
        price: u128,
    },
    Bombed{
        price: u128,
        to_number: Option<u128>,
    },
    Finished{
        winner: ActorId,
    }

}


