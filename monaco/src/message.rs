use crate::*;
use gstd::errors::ContractError;

pub async fn take_your_turn(index_owner: u16, race: &Race) -> Result<Vec<u8>, ContractError> {
    msg::send_for_reply(
        race.players_queue[index_owner as usize],
        YourTurn {
            cars: race.cars_queue.clone(), 
            index_owner: index_owner,
        },
        0,
    )
    .expect("Error on sending `YourTurn` message")
    .up_to(Some(WAIT_DURATION))
    .expect("Invalid wait duration.")
    .await
}