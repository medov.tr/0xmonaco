#![no_std]

use gstd::{exec, msg, prelude::*, ActorId, ReservationId};
use monaco_io::*;
pub mod message;
use message::*;
pub mod strategic_actions;
use gstd::debug;

pub const ZERO_ID: ActorId = ActorId::new([0u8; 32]);
pub const STARTING_BALANCE: u128 = 15_000;
pub const NUMBER_OF_PLAYERS: u16 = 3;
pub const ACCELERATE_TARGET_PRICE: u128 = 100; 
pub const BOMB_TARGET_PRICE: u128 = 100; 
pub const GAS_FOR_ROUND: u64 = 60_000_000_000;
const RESERVATION_AMOUNT: u64 = 245_000_000_000;
pub const RACE_DISTANCE: u128 = 500;
pub const WAIT_DURATION: u32 = 5;
pub const POST_BOMB_SPEED: u128 = 1;

static mut RACE: Option<Race> = None;
static mut RESERVATION: Option<Vec<ReservationId>> = None;

#[derive(Debug, Default, Clone, Encode, Decode, TypeInfo)]
pub struct Race{
    admin: ActorId,
    players_queue: Vec<ActorId>,
    cars_queue: Vec<Car>,
    car_for_owner: BTreeMap<ActorId, Car>,
    state: State,
    current_player: ActorId,
    round: u128,
    winner: ActorId,

}


impl Race{

    fn reserve_gas(&self) {
        unsafe {
            let reservation_id = ReservationId::reserve(RESERVATION_AMOUNT, 864000)
                .expect("reservation across executions");
            let reservations = RESERVATION.get_or_insert(Default::default());
            reservations.push(reservation_id);
        }
        msg::reply(RaceEvent::GasReserved, 0).expect("");
    }

    pub fn register(&mut self, player: &ActorId){

        Self::assert_zero_address(player);

        assert!(!self.car_for_owner.contains_key(player),
            "You have already registered"
        );


        if self.cars_queue.len() > NUMBER_OF_PLAYERS.into() {
            panic!("There should be no more than {} players", NUMBER_OF_PLAYERS);
        }


        let car = Car{number: self.cars_queue.len() as u128 ,balance: STARTING_BALANCE, speed: 0, position: 0};
        self.car_for_owner.insert(*player,car.clone());
        self.players_queue.push(*player);
        self.cars_queue.push(car);
        
        if self.players_queue.len() == NUMBER_OF_PLAYERS.into() {
            self.state = State::PLAY;
        }
        

        msg::reply(RaceEvent::RaceRegistered{ race_id: self.admin, player: player.clone() },
        0,
        ).expect("Error in reply [RaceEvent::RaceRegistered]");

    }

    async fn play(&mut self) {

        assert!(msg::source() == self.admin || msg::source() == exec::program_id(),
            "Only admin or the program can send that message"
        );

        while self.state == State::PLAY {

            if exec::gas_available() <= GAS_FOR_ROUND {
                unsafe {
                    let reservations = RESERVATION.get_or_insert(Default::default());
                    if let Some(id) = reservations.pop() {
                        msg::send_from_reservation(id, exec::program_id(), RaceAction::Play, 0)
                            .expect("Failed to send message");
                        msg::reply(RaceEvent::NextRoundFromReservation, 0).expect("");

                        break;
                    } else {
                        panic!("GIVE ME MORE GAS");
                    };
                }
            }

            self.round = self.round.wrapping_add(1);
            let mut index: u16 = 0;
            for player in self.players_queue.clone(){
                self.current_player = player;
                // debug!("current_player: {:?}", self.current_player);
                
                let state = self.clone();
                let reply = take_your_turn(index.clone(), &state).await;
                if reply.is_err() {
                    panic!("Error in 'take_your_turn' reply");
                }

                // Upgrade 
                for car in &mut self.cars_queue{
                    car.position += car.speed;
                    //debug!(" cars_queue     pos: {:?} speed: {:?}", car.position, car.speed )
                }
                for (_id, car) in &mut self.car_for_owner{
                    car.position += car.speed;
                    //debug!("car_for_owner     pos: {:?} speed: {:?}", car.position, car.speed )
                }

                if let Some(winner) = self.check_position(){
                    self.winner = *winner;
                    self.state = State::FINISHED;
                    debug!("WINNER: {:?}", self.winner);
                    msg::reply(
                        RaceEvent::Finished {
                            winner: self.winner,
                        },
                        0,
                    )
                    .expect("Error in sending a reply `RaceEvent::RaceFinished`");
                    break;
                }

                debug!("cars: {:?}", self.cars_queue);
                //debug!("car_for_owner: {:?}", self.car_for_owner);

                index += 1;
            }

            
            msg::send(
                self.admin,
                RaceEvent::Step {
                    players_queue: self.players_queue.clone(),
                    cars_queue: self.cars_queue.clone(),
                    current_player: self.current_player,
                },
                0,
            )
            .expect("Error in sending a message `GameEvent::Step`");

        }
    }

    

}




#[gstd::async_main]

async fn main() {
    let action: RaceAction = msg::load().expect("Could not load Action");
    let race: &mut Race = unsafe { RACE.get_or_insert(Race::default()) };

    match action {
        RaceAction::Register { player } => race.register(&player),
        RaceAction::ReserveGas => race.reserve_gas(),
        RaceAction::Play => race.play().await,
        RaceAction::BuyAcceleration { speed } => race.buy_acceleration(speed),
        RaceAction::BuyBomb { num_bomb } => race.buy_bomb(num_bomb),
    }


}

#[no_mangle]
extern "C" fn meta_state() -> *mut [i32; 2] {
    let race: &mut Race = unsafe { RACE.get_or_insert(Default::default()) };
    let encoded = race.encode();
    gstd::util::to_leak_ptr(encoded)
}



#[no_mangle]
pub unsafe extern "C" fn init() {
    //let config: InitRace = msg::load().expect("Unable to decode init message");
    let race = Race {
        admin: msg::source(),
        ..Default::default()
    };
    RACE = Some(race);
}
