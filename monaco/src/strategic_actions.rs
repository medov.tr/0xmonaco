use crate::*;

impl Race {
    pub fn check_position(&self) -> Option<&ActorId>{
        let mut winner = None;
        for (id, val) in self.car_for_owner.iter(){
            //debug!("Position: {:?}", val.position );
            if val.position >= RACE_DISTANCE {
                winner = Some(id);
            }
        }
        winner
        

    }

    pub fn buy_acceleration(&mut self, speed: u128){
       // TODO: Improve calculate cost of acceleration
        debug!("BUY ACCELERATION");
        let owner = msg::source();
        if self.car_for_owner.get(&owner) == None{
            panic!("This players not registred")
        }
        let price = Self::get_accelerate_cost(speed.clone());

        if price > self.car_for_owner.get(&owner).unwrap().balance{
            panic!("Not enough money")
        }

        let mut index = 0;
        self.car_for_owner
        .entry(owner)
        .and_modify(|car| {
            index = car.number;
            car.balance -= price;
            car.speed += speed;

        });

        if let Some(car) =  self.cars_queue.get_mut(index as usize){
            car.balance -= price.clone();
            car.speed += speed;
        }
        


        msg::reply(RaceEvent::Accelerated { price: price},
        0,
        ).expect("Error in reply [RaceEvent::Accelerated]");

    }


    pub fn buy_bomb(&mut self, num_bomb: u128){
        // TODO: Improve calculate cost of bomb
        debug!("BUY BOMB");
        assert!(num_bomb != 0, "You cant buy zero bomb");
        let owner = msg::source();
        if self.car_for_owner.get(&owner) == None{
            panic!("This players not registred")
        }
        let price = Self::get_bomb_cost(num_bomb.clone());

        let mut index = 0;
        self.car_for_owner
        .entry(owner)
        .and_modify(|car| {
            index = car.number;
            car.balance -= price;
        });

        let car = self.cars_queue.get_mut(index as usize).unwrap();
        car.balance -= price.clone();


        let closest_car = self.closest_car_ahead(index as usize); // what to do if there is no car in front
        //debug!("closest_car: {:?}", closest_car);
        if let Some(closest_index) = closest_car{


            for (_id, car) in &mut self.car_for_owner{
                if car.number == closest_index{
                    car.speed = POST_BOMB_SPEED;
                }
            }


            if let Some(car) = self.cars_queue.get_mut(closest_index as usize){
                car.speed = POST_BOMB_SPEED;
            }

            
        }

        msg::reply(RaceEvent::Bombed { price: price, to_number: closest_car},
        0,
        ).expect("Error in reply [RaceEvent::Accelerated]");
 
     }


    fn closest_car_ahead(&self, idx: usize) -> Option<u128>{
        let mut closest_car = None;
        let mut dist = 1000;
        for car in self.cars_queue.iter(){
            if *car == *self.cars_queue.get(idx).unwrap(){
                continue;
            }
            else{
                
                let owner_position = self.cars_queue.get(idx).unwrap().position;
                //debug!("owner_position: {:?}, other: {:?} ", owner_position, car.position);
                if car.position > owner_position{
                    if (car.position - owner_position) <= dist{
                        //debug!("I AM HERE");
                        dist = car.position - owner_position;
                        closest_car = Some(car.number);
                    }
                }
            }
        }
        closest_car

    }

    fn get_accelerate_cost(speed: u128) -> u128 {
        // TODO: improve this calculate
        speed * ACCELERATE_TARGET_PRICE
    }
    fn get_bomb_cost(num_bomb: u128) -> u128 {
        // TODO: improve this calculate
        num_bomb * BOMB_TARGET_PRICE
    }

    pub fn assert_zero_address(addresses: &ActorId){
        if *addresses == ZERO_ID{
            panic!("Zero address");
            
        }
    }
}